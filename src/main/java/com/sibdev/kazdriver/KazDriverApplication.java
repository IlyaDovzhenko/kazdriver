package com.sibdev.kazdriver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KazDriverApplication {
	public static void main(String[] args) {
		SpringApplication.run(KazDriverApplication.class, args);
	}
}
