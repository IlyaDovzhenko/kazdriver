package com.sibdev.kazdriver.repository;

import com.sibdev.kazdriver.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {

}
